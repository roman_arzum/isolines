typedef struct Flat_Map
{
	int p_x, p_y;
	float depth;
	
	//Alignment
	char padding[4];
} Flat_Map;

typedef union My_Map
{
	Flat_Map structure;
	uchar16 vector;
} My_Map;

typedef uchar flat_map_data;

const sampler_t Grayscale_Sampler = 
	CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;

void __kernel Export(
	__global const flat_map_data   *flat_map,
	__write_only image2d_t         img_out)
{
	const size_t pos = get_global_id(0);
	
	My_Map bath_point;
	bath_point.vector  = vload16(pos, flat_map);

	//Export information about measured depth points
	int2 img_point = {bath_point.structure.p_x, bath_point.structure.p_y};
	float4 depth = bath_point.structure.depth;
	write_imagef(img_out, img_point, depth);
	
	return;
}

void __kernel Laplace_Round(
	__global const flat_map_data	*flat_map,
	__read_only image2d_t         	shore_mask,
	__read_only image2d_t         	img_in,
	__write_only image2d_t         	img_out,
	__global float					*interim)
{
	const int2
		pos = {get_global_id(0), get_global_id(1)},
		l_p = {pos.x-1, pos.y},
		r_p = {pos.x+1, pos.y},
		d_p = {pos.x, pos.y-1},
		u_p = {pos.x, pos.y+1};
	
	//If current pixel belongs to shore, depth is zero
	if(read_imageui(shore_mask, Grayscale_Sampler, pos).s0 == 0){
		float4 depth=0;
		write_imagef(img_out, pos, depth);
		return;
	}
	
	float4 depth = 
		   (read_imagef(img_in, Grayscale_Sampler, l_p)+
			read_imagef(img_in, Grayscale_Sampler, r_p)+
			read_imagef(img_in, Grayscale_Sampler, d_p)+
			read_imagef(img_in, Grayscale_Sampler, u_p)+
			read_imagef(img_in, Grayscale_Sampler, pos))/5;
	
	write_imagef(img_out, pos, depth);
	return;
}