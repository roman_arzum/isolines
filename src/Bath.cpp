#include <fstream>
#include "Bath.hpp"
#include "CLSteelThread.hpp"
#include "DelaunayRect2D.hpp"

using namespace delaunay;

namespace bath
{
    Bath::Bath(
        CLSteelThread               &steel_thread,
        delaunay::DelaunayRect2D    &area,
        const std::string           &bath_name,
        const std::string           &path_to_kernels) throw (cl::Error) :

        cv_img(area.region.get_Y(), area.region.get_X(), CV_32F, cv::Scalar(0.0f)),
        cl_img(steel_thread.get_Context(), CL_MEM_ALLOC_HOST_PTR, cl::ImageFormat(CL_LUMINANCE, CL_FLOAT), area.region.get_X(), area.region.get_Y()),
        
        path_to_src(path_to_kernels),
        src_code(std::istreambuf_iterator<char>(std::ifstream(Bath::path_to_src.c_str())), (std::istreambuf_iterator<char>())),
        
        st_thread(steel_thread),
        delaunay_area(area),
        name(bath_name),
    
        prg_src(1, std::make_pair(src_code.c_str(), src_code.length() + 1)),
        prg(steel_thread.get_Context(), prg_src),

        width(area.region.get_X()),
        height(area.region.get_Y())
{
    try{
        this->prg.build(this->st_thread.get_Devices(), "");
    }
    catch(cl::Error &e){
        //Get programm build log (shows OpenCL source code errors)
        if(e.err() == CL_BUILD_PROGRAM_FAILURE){
            std::cerr <<"Build log:"<<std::endl<<
            this->prg.getBuildInfo<CL_PROGRAM_BUILD_LOG>(st_thread.get_Devices()[0])
            <<std::endl;
        }
        throw e;
    }
}

Bath::~Bath(void)
{}

std::size_t Bath::GetSize(void) const
{
    return this->width * this->height * sizeof(float);
}

void Bath::SaveToMat(void)
{
    try{
        cl::size_t<3> origin, region;
        origin[0] = origin[1] = origin[2] = 0;
        region[0] = this->width;
        region[1] = this->height;
        region[2] = 1;

        st_thread.get_Htod().enqueueReadImage(this->cl_img,
            CL_TRUE, origin, region, 0, 0, (void*)this->cv_img.data);
    }
    catch (cl::Error &e){
        CryErr(e);
        throw e;
    }
}

void Bath::Show(void)
{
    cv::namedWindow(this->name, cv::WINDOW_NORMAL);
    cv::imshow(this->name, this->cv_img);
    cv::waitKey(0);
}

cl::Image2D& Bath::GetImgCL(void)
{
    return this->cl_img;
}

const cl::Image2D& Bath::GetImgCL(void) const
{
    return this->cl_img;
}

cv::Mat& Bath::GetImgCV(void)
{
    return this->cv_img;
}

const cv::Mat& Bath::GetImgCV(void) const
{
    return this->cv_img;
}

}
