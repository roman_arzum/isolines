#pragma once
#define __CL_ENABLE_EXCEPTIONS
#include <string>
#include <CL/cl.hpp>
#include <opencv2/opencv.hpp>

#include "Utils.hpp"
#include "DelaunayRect2D.hpp"

class CLSteelThread;

namespace delaunay{
    class DelaunayRect2D;
}

namespace bath
{
class Bath
{
protected:
    //Floating point image on Host side
    cv::Mat cv_img;

    //Same image on Device side
    cl::Image2D cl_img;

    //OpenCL program, it's sources & bath name (to show in highgui window)
    std::string 
        path_to_src, 
        src_code, 
        name;
    
    cl::Program::Sources prg_src;
    cl::Program prg;
    
    //Image width & height in pixels
    std::size_t width, height;
    
    CLSteelThread &st_thread;
    delaunay::DelaunayRect2D delaunay_area;

public:
    Bath(
        CLSteelThread               &steel_thread,
        delaunay::DelaunayRect2D    &area,
        const std::string           &bath_name,
        const std::string           &path_to_kernels) throw (cl::Error);

    virtual ~Bath(void);

    virtual Bath& operator = (Bath rhs)
    {
        std::swap(*this, rhs);
        return *this;
    }

    virtual std::size_t GetSize(void) const;

    virtual void SaveToMat(void);

    virtual void Show(void);

    virtual cl::Image2D& GetImgCL(void);

    virtual const cl::Image2D& GetImgCL(void) const;

    virtual cv::Mat& GetImgCV(void);

    virtual const cv::Mat& GetImgCV(void) const;
};
}

