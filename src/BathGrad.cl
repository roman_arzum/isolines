const sampler_t ImgSampler = 
	CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;

int to_1d(int2 position, int linesize)
{
	return position.x + position.y * linesize;
}

void __kernel CalcGradients(
	__read_only image2d_t   img_in,
	__global float			*interim_gx,
	__global float			*interim_gy,
	__global float			*interim_ox,
	__global float			*interim_oy)
{
	const int2
		p1 = {get_global_id(0),   get_global_id(1)},
		p2 = {get_global_id(0)+1, get_global_id(1)},
		p3 = {get_global_id(0),   get_global_id(1)+1},
		p4 = {get_global_id(0)+1, get_global_id(1)+1};

	//Interim buffer linesize
	const int linesize = get_global_size(0);

	float4 pix_in[4] = {
		read_imagef(img_in, ImgSampler, p1),
		read_imagef(img_in, ImgSampler, p2),
		read_imagef(img_in, ImgSampler, p3),
		read_imagef(img_in, ImgSampler, p4)
	};

	float
		gx = pix_in[1].s0 - pix_in[0].s0,
		gy = pix_in[2].s0 - pix_in[0].s0,
		ox = gx*gx - gy*gy,
		oy = 2*gx*gy;

	interim_gx[to_1d(p1, linesize)] = gx;
	interim_gy[to_1d(p1, linesize)] = gy;
	interim_ox[to_1d(p1, linesize)] = ox;
	interim_oy[to_1d(p1, linesize)] = oy;
		
	return;
}

void __kernel CalcOrients(
	__global float			*interim_gx,
	__global float			*interim_gy,
	__global float			*interim_ox,
	__global float			*interim_oy)
{
	const int2 p5 = {get_global_id(0), get_global_id(1)};

	//Interim buffer linesize
	const int linesize = get_global_size(0);

	float
		gx = interim_gx[to_1d(p5, linesize)],
		gy = interim_gy[to_1d(p5, linesize)],
		ox = gx*gx - gy*gy,
		oy = 2*gx*gy;

	interim_ox[to_1d(p5, linesize)] = ox;
	interim_oy[to_1d(p5, linesize)] = oy;
		
	return;
}

void __kernel ProjectOrients(
	__global float			*interim_gx,
	__global float			*interim_gy,
	__global float			*interim_ox,
	__global float			*interim_oy)
{
	const int2 p5 = {get_global_id(0), get_global_id(1)};

	//Interim buffer linesize
	const int linesize = get_global_size(0);

	float
		gx = interim_gx[to_1d(p5, linesize)],
		gy = interim_gy[to_1d(p5, linesize)],
		ox = interim_ox[to_1d(p5, linesize)],
		oy = interim_oy[to_1d(p5, linesize)],
		a  = atan2(oy, ox);

	ox = cos(a/2);
	oy = sin(a/2);

	float k = gx * ox + gy * oy;
	gx = ox * k;
	gy = oy * k;

	interim_gx[to_1d(p5, linesize)] = gx;
	interim_gy[to_1d(p5, linesize)] = gy;
		
	return;
}

#define BLACK	(0)
#define RED		(1)

void __kernel LaplaceRound(
	__global float					*interim,
	__global float					*deltas,
	const uint						linesize)
{
	int2 
		pos  = {get_global_id(0)*2, get_global_id(1)},
		dims = {linesize, get_global_size(1)};

	float delta[2] = {0.0f, 0.0f};

	//Dimension, needed to search maximal delta
	int 
		rows  = dims.y / 2, 
		cols  = linesize / 2, 
		width = linesize;

	#pragma unroll
	for(int color = BLACK; color <= RED; color++){
		/* At black cells at even rows stay, cells at odd rows go right.
		 * At red   cells at even rows go right, cells at odd rows go left. */
		pos.x	     += (1 - color)*(pos.y%2) + color*(1 - 2*(pos.y%2));
		int2 up	   = {pos.x, pos.y-1};
		int2 down	 = {pos.x, pos.y+1}; 
		int2 left	 = {pos.x-1, pos.y}; 
		int2 right = {pos.x+1, pos.y};

		//Make out-of bounds operations stick to border
		if(pos.x == 0)			{left  = pos;}
		if(pos.x == dims.x-1)	{right = pos;}
		if(pos.y == 0)			{up    = pos;}
		if(pos.y == dims.y-1)	{down  = pos;}

		delta[color] = interim[to_1d(pos, dims.x)];	

		interim[to_1d(pos, dims.x)] = (
			interim[to_1d(up,    dims.x)] +
			interim[to_1d(down,  dims.x)] +
			interim[to_1d(left,  dims.x)] +
			interim[to_1d(right, dims.x)]) / 4;

		delta[color] -= interim[to_1d(pos, dims.x)];		

		barrier(CLK_GLOBAL_MEM_FENCE);
	}

	//Finding maximal delta value
	delta[0] = fabs(delta[0]);
	delta[1] = fabs(delta[1]);

	if (delta[0] < delta[1]) {delta[0] = delta[1];}

	//Write individual deltas to global memory
	pos = (int2)(get_global_id(0), get_global_id(1));
	deltas[to_1d(pos, width)] = delta[0];
	barrier(CLK_GLOBAL_MEM_FENCE);

	//Compare own delta with neighbour's delta, swap if own is less
	while(rows){
		int2 neighbor = {pos.x, pos.y+rows};
		bool less_delta = (pos.y < rows) && 
			(deltas[to_1d(pos, width)] < deltas[to_1d(neighbor, width)]);		

		if((pos.y < rows) && less_delta){
			deltas[to_1d(pos, width)] = deltas[to_1d(neighbor, width)];
		}

		barrier(CLK_GLOBAL_MEM_FENCE);
		rows = rows / 2;
	}



	while(cols){
		int2 neighbor = {pos.x+cols, pos.y};
		bool less_delta = (pos.x < cols) && 
			(deltas[to_1d(pos, width)] < deltas[to_1d(neighbor, width)]);
		
		if(less_delta){
			deltas[to_1d(pos, width)] = deltas[to_1d(neighbor, width)];
		}

		barrier(CLK_GLOBAL_MEM_FENCE);
		cols = cols / 2;
	}

	return;
}