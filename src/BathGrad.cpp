#include "DelaunayRect2D.hpp"
#include "CLSteelThread.hpp"
#include "BathGrad.hpp"
#include "BathMap.hpp"
#include <iostream>
#include <fstream>
#include <string>

namespace bath
{
const std::string BathGrad::path_to_kernels("../BathGrad.cl");
const float BathGrad::delta = 5e-7f;

BathGrad::BathGrad(
    CLSteelThread				&steel_thread, 
    delaunay::DelaunayRect2D	&delaunay_area,
    BathMap						&bath_map):

    Bath(steel_thread, delaunay_area, std::string("Gradient Map"), path_to_kernels),
    map(bath_map),
    calc_grads(Bath::prg, "CalcGradients"),
    calc_orients(Bath::prg, "CalcOrients"),
    project_orients(Bath::prg, "ProjectOrients"),
    smooth_round(Bath::prg, "LaplaceRound")
    
{
    try{
        this->grad_x = cl::Buffer(steel_thread.get_Context(), CL_MEM_ALLOC_HOST_PTR,
            bath_map.GetSize());

        this->grad_y = cl::Buffer(steel_thread.get_Context(), CL_MEM_ALLOC_HOST_PTR,
            bath_map.GetSize());

        this->orient_x = cl::Buffer(steel_thread.get_Context(), CL_MEM_ALLOC_HOST_PTR,
            bath_map.GetSize());

        this->orient_y = cl::Buffer(steel_thread.get_Context(), CL_MEM_ALLOC_HOST_PTR,
            bath_map.GetSize());

        this->deltas = cl::Buffer(steel_thread.get_Context(), CL_MEM_ALLOC_HOST_PTR,
            bath_map.GetSize());
    } catch(cl::Error &e) {
        CryErr(e);
        throw e;
    }
}

BathGrad::~BathGrad(void)
{}

cv::Mat BathGrad::NormalizeGradients(
    bool    x_or_y,
    bool    to_positive)
{
    cv::Mat normalized(this->height, this->width, CV_32F, cv::Scalar(0.0f));
    
    try{
        cl::Buffer *p_buffer = (x_or_y) ? (&this->grad_x) : (&this->grad_y);

        this->st_thread.get_Htod().enqueueReadBuffer(*p_buffer,
            CL_TRUE, 0, this->GetSize(), (void*)normalized.data);

        //Find maximal value
        float factor = 0.0f;
        float *pixels = (float*)normalized.data;

        for (std::size_t i = 0; i<this->height * this->width; i++){
            if (fabs(pixels[i]) > factor){
                factor = fabs(pixels[i]);
            }
        }

        //Normalize image
        for (std::size_t i = 0; i<this->height * this->width; i++){
            pixels[i] /= factor;
                
            if(to_positive){
                pixels[i] += 1;
                pixels[i] /= 2;
            }
        }
    } catch (cl::Error &e) {
        CryErr(e);
        throw e;
    }

    return normalized;
}

void BathGrad::SmoothInterim(cl::Buffer &buffer, float given_delta)
{
    if (&buffer != &this->grad_x &&
        &buffer != &this->grad_y &&
        &buffer != &this->orient_x &&
        &buffer != &this->orient_y){

        cl::Error err(1, "Given buffer doesn't reference to class member");
        CryErr(err);
        throw err;
    }

    //Smoothing 2 interim buffers
    try{
        int it_count = 0;
        unsigned int linesize = (unsigned int)this->width;

        float 
            delta = given_delta + 1.0f,
            delta1 = delta,
            delta2 = delta1;

        while (delta >= given_delta){
            this->smooth_round.setArg<cl::Buffer>(0, buffer);
            this->smooth_round.setArg<cl::Buffer>(1, this->deltas);
            this->smooth_round.setArg(2, sizeof(unsigned int), &linesize);

            cl::NDRange GlobalSize(this->width >> 1, this->height);
            cl::Event kernel_ready;

            //First run
            this->st_thread.get_Cmd().enqueueNDRangeKernel(this->smooth_round,
            cl::NullRange, GlobalSize, cl::NullRange, 0, &kernel_ready);
            kernel_ready.wait();

            //Read one float from GPU, it contains max delta value
            this->st_thread.get_Dtoh().enqueueReadBuffer(this->deltas, CL_TRUE, 0,
                sizeof(float), (void*)&delta1);

            //Second run to get deltas difference
            this->st_thread.get_Cmd().enqueueNDRangeKernel(this->smooth_round,
            cl::NullRange, GlobalSize, cl::NullRange, 0, &kernel_ready);
            kernel_ready.wait();

            this->st_thread.get_Dtoh().enqueueReadBuffer(this->deltas, CL_TRUE, 0,
                sizeof(float), (void*)&delta2);

            delta = fabs(delta1 - delta2);
            std::cout << "\tIteration # " << it_count << ". Delta: " << delta << " / " << given_delta << std::endl;
            it_count += 2;
        }
    }catch (cl::Error &e) {
        CryErr(e);
        throw e;
    }
}

void BathGrad::Calculate(float given_delta)
{
    //Calculating gradients from image to interim buffer
    try{
        this->calc_grads.setArg<cl::Image2D>(0, this->map.cl_img);
        this->calc_grads.setArg<cl::Buffer>(1, this->grad_x);
        this->calc_grads.setArg<cl::Buffer>(2, this->grad_y);
        this->calc_grads.setArg<cl::Buffer>(3, this->orient_x);
        this->calc_grads.setArg<cl::Buffer>(4, this->orient_y);

        cl::NDRange GlobalSize(this->width, this->height);
        cl::Event kernel_ready;

        this->st_thread.get_Cmd().enqueueNDRangeKernel(this->calc_grads,
            cl::NullRange, GlobalSize, cl::NullRange, 0, &kernel_ready);
        kernel_ready.wait();
    }catch (cl::Error &e) {
        CryErr(e);
        throw e;
    }

    //Calculating orientations
    try{
        this->calc_orients.setArg<cl::Buffer>(0, this->grad_x);
        this->calc_orients.setArg<cl::Buffer>(1, this->grad_y);
        this->calc_orients.setArg<cl::Buffer>(2, this->orient_x);
        this->calc_orients.setArg<cl::Buffer>(3, this->orient_y);

        cl::NDRange GlobalSize(this->width, this->height);
        cl::Event kernel_ready;

        this->st_thread.get_Cmd().enqueueNDRangeKernel(this->calc_orients,
            cl::NullRange, GlobalSize, cl::NullRange, 0, &kernel_ready);
        kernel_ready.wait();

        //Smoothing orientations
        this->SmoothInterim(this->orient_x, given_delta);
        this->SmoothInterim(this->orient_y, given_delta);
    }catch (cl::Error &e) {
        CryErr(e);
        throw e;
    }

    //Project orientations to gradients
    try{
        this->project_orients.setArg<cl::Buffer>(0, this->grad_x);
        this->project_orients.setArg<cl::Buffer>(1, this->grad_y);
        this->project_orients.setArg<cl::Buffer>(2, this->orient_x);
        this->project_orients.setArg<cl::Buffer>(3, this->orient_y);

        cl::NDRange GlobalSize(this->width, this->height);
        cl::Event kernel_ready;

        this->st_thread.get_Cmd().enqueueNDRangeKernel(this->project_orients,
            cl::NullRange, GlobalSize, cl::NullRange, 0, &kernel_ready);
        kernel_ready.wait();

        //Smoothing gradients
        this->SmoothInterim(this->grad_x, given_delta);
        this->SmoothInterim(this->grad_y, given_delta);
    }catch (cl::Error &e) {
        CryErr(e);
        throw e;
    }
}

void BathGrad::SaveToFile(
    const std::string	&filename_x,
    const std::string	&filename_y)
{
    cv::Mat img(this->height, this->width, CV_8U);

    for(int file=0; file<2; file++){
        try{
            cv::Mat mat = this->NormalizeGradients((bool)file, true);
            
            //Convert image to unsigned chars
            for (std::size_t i = 0; i< mat.cols * mat.rows; i++){
                img.data[i] = ((float*)mat.data)[i] * 255;
            }

            if (!file){
                cv::imwrite(filename_x, img);
            } else{
                cv::imwrite(filename_y, img);
            }
        } catch (cl::Error &e) {
            CryErr(e);
            throw e;
        }
    }
}

cl::Buffer& BathGrad::GetGradX(void)
{
    return this->grad_x;
}

cl::Buffer& BathGrad::GetGradY(void)
{
    return this->grad_y;
}

}