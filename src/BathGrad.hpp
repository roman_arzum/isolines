#pragma once
#include <string>
#include <vector>
#include "Bath.hpp"

namespace bath
{
class BathMap;
class BathGrad: public Bath
{
protected:
    friend class BathMap;
    
    BathMap &map;
    
    cl::Kernel 
        calc_grads, 
        calc_orients, 
        project_orients, 
        smooth_round;

    cl::Buffer
        grad_x,
        grad_y,
        orient_x,
        orient_y,
        deltas;
    
    static const float delta;
    static const std::string path_to_kernels;

public:

    BathGrad(
        CLSteelThread				&steel_thread, 
        delaunay::DelaunayRect2D	&delaunay_area,
        BathMap						&bath_map);

    ~BathGrad(void);

    BathGrad& operator = (BathGrad rhs)
    {
        std::swap(*this, rhs);
        return *this;
    }

    void Calculate(float given_delta);

    cv::Mat NormalizeGradients(
        bool    x_or_y,
        bool    to_positive);

    void SmoothInterim(cl::Buffer &buffer, float given_delta);

    cl::Buffer& GetGradX(void);

    cl::Buffer& GetGradY(void);

    void SaveToFile(
        const std::string   &filename_x,
        const std::string	&filename_y);
};
}

