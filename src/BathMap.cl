typedef struct Flat_Map
{
	int p_x, p_y;
	float depth;
} Flat_Map;

const sampler_t Grayscale_Sampler = 
	CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;

#define BLACK	(0)
#define RED		(1)

int to_1d(int2 position, int linesize)
{
	return position.x + position.y * linesize;
}

void __kernel Laplace_Round(
	__read_only image2d_t 			weights,
	__global float					*interim,
	__global float					*deltas)
{
	int2 
		pos  = {get_global_id(0)*2, get_global_id(1)},
		dims = get_image_dim(weights);

	float delta[2] = {0.0f, 0.0f};

	//Dimension, needed to search maximal delta
	int 
		rows  = get_global_size(1) / 2, 
		cols  = get_global_size(0) / 2, 
		width = dims.x;

	#pragma unroll
	for(int color = BLACK; color <= RED; color++){
		/* At black cells at even rows stay, cells at odd rows go right.
		 * At red   cells at even rows go right, cells at odd rows go left. */
		pos.x	    += (1 - color)*(pos.y%2) + color*(1 - 2*(pos.y%2));
		int2 up	    = {pos.x, pos.y-1};
		int2 down	= {pos.x, pos.y+1}; 
		int2 left	= {pos.x-1, pos.y}; 
		int2 right	= {pos.x+1, pos.y};

		//Make out-of bounds operations stick to border
		if(pos.x == 0)			{left  = pos;}
		if(pos.x == dims.x-1)	{right = pos;}
		if(pos.y == 0)			{up    = pos;}
		if(pos.y == dims.y-1)	{down  = pos;}

		//Point, which depth is measured & shouldn't be approximated
		bool fixed_point = 
			(read_imagef(weights, Grayscale_Sampler, pos).s0 == 1.0f) ||
			(read_imagef(weights, Grayscale_Sampler, pos).s0 == 0.0f);

		if(!fixed_point){
			delta[color] = interim[to_1d(pos, dims.x)];
		
			interim[to_1d(pos, dims.x)] = (
				interim[to_1d(up,    dims.x)] +
				interim[to_1d(down,  dims.x)] +
				interim[to_1d(left,  dims.x)] +
				interim[to_1d(right, dims.x)]) / 4;

			delta[color] -= interim[to_1d(pos, dims.x)];
		}
		
		barrier(CLK_GLOBAL_MEM_FENCE);
	}

	//Finding maximal delta value
	delta[0] = fabs(delta[0]);
	delta[1] = fabs(delta[1]);
	if (delta[0] < delta[1]) {delta[0] = delta[1];}

	//Write individual deltas to global memory
	pos = (int2)(get_global_id(0), get_global_id(1));
	deltas[to_1d(pos, width)] = delta[0];
	barrier(CLK_GLOBAL_MEM_FENCE);
	
	//Compare own delta with neighbour's delta, swap if own is less
	while(rows){
		int2 neighbor = {pos.x, pos.y+rows};
		bool less_delta = (pos.y < rows) && 
			(deltas[to_1d(pos, width)] < deltas[to_1d(neighbor, width)]);
		
		if((pos.y < rows) && less_delta){
			deltas[to_1d(pos, width)] = deltas[to_1d(neighbor, width)];
		}
		barrier(CLK_GLOBAL_MEM_FENCE);
		rows = rows / 2;
	}

	while(cols){
		int2 neighbor = {pos.x+cols, pos.y};
		bool less_delta = (pos.x < cols) && 
			(deltas[to_1d(pos, width)] < deltas[to_1d(neighbor, width)]);
		
		if(less_delta){
			deltas[to_1d(pos, width)] = deltas[to_1d(neighbor, width)];
		}
		barrier(CLK_GLOBAL_MEM_FENCE);
		cols = cols / 2;
	}

	return;
}

