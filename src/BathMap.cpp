#include "BathMap.hpp"
#include "DelaunayRect2D.hpp"

#include <exception>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <math.h>
#include <omp.h>

using namespace delaunay;
using namespace std;

namespace bath
{
const float BathMap::delta = 5e-7f;
const std::string BathMap::path_to_kernels("../BathMap.cl");

BathMap::BathMap(
        cv::Mat			&mask, 
        DelaunayRect2D  &rect2d, 
        CLSteelThread	&steel_thread):

        Bath(steel_thread, rect2d, std::string("Bath map"), path_to_kernels),
        shore_mask(mask),

        shore_img(steel_thread.get_Context(), CL_MEM_ALLOC_HOST_PTR, cl::ImageFormat(CL_LUMINANCE, CL_FLOAT), rect2d.GetWidth(), rect2d.GetHeight()),
        weights_img(steel_thread.get_Context(), CL_MEM_ALLOC_HOST_PTR, cl::ImageFormat(CL_LUMINANCE, CL_FLOAT), rect2d.GetWidth(), rect2d.GetHeight()),

        points_buf(st_thread.get_Context(), CL_MEM_ALLOC_HOST_PTR | CL_MEM_COPY_HOST_PTR, rect2d.points.size() * sizeof(Point2D), (void*)&rect2d.points[0]),
        interim_buffer(st_thread.get_Context(), CL_MEM_ALLOC_HOST_PTR, rect2d.GetAreaSize() * sizeof(float)),
        deltas(st_thread.get_Context(), CL_MEM_ALLOC_HOST_PTR, rect2d.GetAreaSize() * sizeof(float)),

        k_laplace_round(this->prg, "Laplace_Round")
{}

void BathMap::MakeInitApprox(void) throw(cl::Error)
{
    float *mapped_mem;
    //Map GPU memory to initialize depth & shore values
    try{
        mapped_mem = (float*)this->st_thread.get_Dtoh().enqueueMapBuffer(this->interim_buffer,
            CL_TRUE, CL_MAP_WRITE_INVALIDATE_REGION, 0, this->GetSize());
    }catch(cl::Error &e) {throw e;}

    //Walk through all triangles & calculate 3-point gradient within for every point
    std::vector<DelaunayTriangle2D>::const_iterator it = this->delaunay_area.triangles.begin();
    int p_index = 0;
    for(; it != this->delaunay_area.triangles.end(); it++){
        DelaunayRect2D bbox = it->GetBBox();
        int i, j;
        
        //Calculate depth
        for(i = bbox.origin.get_Y(); i < bbox.region.get_Y(); i++){
            for(j = bbox.origin.get_X(); j < bbox.region.get_X(); j++){
                delaunay::Point2D c_p(j, i);
                if(it->PointInside(c_p)){
                    //No need to process shore at all
                    if(this->shore_mask.data[j + i * this->shore_mask.cols] == 0){
                        continue;
                    }

                    //Depth is normalized float value within range [0.0f ... 1.0f]
                    float depth = it->TPG(c_p) / this->delaunay_area.GetMaxVal();
                    mapped_mem[j + i * this->shore_mask.cols] = depth;
                }
            }
        }

        #if 0
        //Make pre-smoothed surface patch
        for (int turn = 0; turn < 16; turn++){
            for (i = bbox.origin.get_Y(); i < bbox.region.get_Y(); i++){
                for (j = bbox.origin.get_X(); j < bbox.region.get_X(); j++){
                    delaunay::Point2D c_p(j, i);
                    if (it->PointInside(c_p) && !it->PointIsVertex(c_p)){
                        mapped_mem[j + i * this->shore_mask.cols] = (
                            mapped_mem[(j-1) + i * this->shore_mask.cols] +
                            mapped_mem[(j+1) + i * this->shore_mask.cols] +
                            mapped_mem[j + (i-1) * this->shore_mask.cols] +
                            mapped_mem[j + (i+1) * this->shore_mask.cols]) / 4;
                    }
                }
            }
        }
        #endif

        std::cout<<p_index<<" of "<<this->delaunay_area.triangles.size()<<" triangles processed"<<std::endl;
        p_index++;
    }

    //Unmap memory to make it available on GPU side
    try{
        cl::Event unmap_ready;
        this->st_thread.get_Htod().enqueueUnmapMemObject(this->interim_buffer, (void*)mapped_mem,
            0, &unmap_ready);
        unmap_ready.wait();
    } catch(cl::Error &e){
        CryErr(e);
        throw e;
    }
}

void BathMap::CalcWeightOnHost(void)
{
    float *mapped_weights;
    //Map GPU memory to initialize depth & shore values
    try{
        std::size_t row_pitch, slice_pitch;
        cl::size_t<3> origin, region;
        origin[0] = origin[1] = origin[2] = 0;
        region[0] = this->width;
        region[1] = this->height;
        region[2] = 1;

        mapped_weights = (float*)this->st_thread.get_Dtoh().enqueueMapImage(this->weights_img,
            CL_TRUE, CL_MAP_WRITE_INVALIDATE_REGION, origin, region, &row_pitch, &slice_pitch);
    }
    catch (cl::Error &e) { throw e; }

    //Walk through all triangles & calculate 3-point weight gradient within for every point
    //std::vector<DelaunayTriangle2D>::const_iterator it = this->delaunay_area.triangles.begin();
    #pragma omp parallel for
    for (std::vector<DelaunayTriangle2D>::const_iterator it = this->delaunay_area.triangles.begin(); it != this->delaunay_area.triangles.end(); it++){
        DelaunayRect2D bbox = it->GetBBox();

        for (int i = bbox.origin.get_Y(); i < bbox.region.get_Y(); i++){
            for (int j = bbox.origin.get_X(); j < bbox.region.get_X(); j++){

                delaunay::Point2D c_p(j, i);
                if (it->PointInside(c_p)){
                    //No need to process shore at all
                    if (this->shore_mask.data[j + i * this->shore_mask.cols] == 0){
                        mapped_weights[j + i * this->shore_mask.cols] = 0.0f;
                        continue;
                    }

                    //Calculate point weight
                    float weight = it->WeightTPG(c_p);
                    mapped_weights[j + i * this->shore_mask.cols] = weight;
                }
            }
        }
    }

    //Unmap memory to make it available on GPU side
    try{
        cl::Event unmap_ready;
        this->st_thread.get_Htod().enqueueUnmapMemObject(this->weights_img, mapped_weights, 0, &unmap_ready);
        unmap_ready.wait();
    } catch (cl::Error &e){
        CryErr(e);
        throw e; 
    }
}

float BathMap::LaplaceRound(void) throw(cl::Error)
{
    float my_delta = 0;
    try{
        this->k_laplace_round.setArg<cl::Image2D>(0, this->weights_img);
        this->k_laplace_round.setArg<cl::Buffer> (1, this->interim_buffer);
        this->k_laplace_round.setArg<cl::Buffer> (2, this->deltas);
        
        cl::NDRange GlobalSize(this->width / 2, this->height);
        
        this->st_thread.get_Cmd().enqueueNDRangeKernel(this->k_laplace_round,
            cl::NullRange, GlobalSize, cl::NullRange);

        //Read one float from GPU, it contains max delta value
        //use same queue as kernel does to avoid waiting for kernel
        this->st_thread.get_Cmd().enqueueReadBuffer(this->deltas, CL_TRUE, 0,
            sizeof(float), (void*)&my_delta);

        /*
        this->CopyToImg();
        this->SaveToMat();
        this->Show();*/

    } catch (cl::Error &e){
        CryErr(e);
        throw e;
    }

    return fabs(my_delta);
}

void BathMap::Calculate(float given_delta)
{
    int i = 0;
    float c_delta = given_delta + 1.0f;

    while (c_delta >= given_delta){
        c_delta = fabs(this->LaplaceRound() - this->LaplaceRound());
        std::cout << "Iteration # " << i << ". Delta: " << c_delta << " / " << given_delta << std::endl;
        i+=2;
    }
}

void BathMap::CopyToImg(void)
{
    try{
        cl::size_t<3> origin, region;
        origin[0] = origin[1] = origin[2] = 0;
        region[0] = this->width;
        region[1] = this->height;
        region[2] = 1;

        cl::Event evt;
        st_thread.get_Htod().enqueueCopyBufferToImage(this->interim_buffer, this->cl_img,
            0, origin, region, NULL, &evt);
        evt.wait();
    }
    catch (cl::Error &e){
        CryErr(e);
        throw e;
    }
}

};
