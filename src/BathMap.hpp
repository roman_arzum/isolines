#pragma once
#include "CLSteelThread.hpp"
#include "Bath.hpp"
#include <opencv2/opencv.hpp>
#include <CL/cl.hpp>
#include <string>
#include <vector>

namespace delaunay
{
class DelaunayRect2D;
}

struct Flat_Map {
    unsigned int p_x, p_y;
    float depth;
};

namespace bath
{
class BathMap: public Bath
{
    friend class BathGrad;
    friend class ImgPyramid;
    friend class GradientImage;

    cv::Mat shore_mask;
    
    cl::Image2D 
        shore_img, 
        weights_img;
    
    cl::Buffer 
        points_buf, 
        interim_buffer, 
        deltas;
    
    cl::Kernel k_laplace_round;

    static const float delta;
    static const std::string path_to_kernels;
    
public:
    BathMap(
        cv::Mat						&mask, 
        delaunay::DelaunayRect2D    &rect2d, 
        CLSteelThread				&steel_thread);

    ~BathMap(void)
    {}

    BathMap& operator = (BathMap rhs)
    {
        std::swap(*this, rhs);
        return *this;
    }

    void MakeInitApprox() throw(cl::Error);

    float LaplaceRound(void) throw(cl::Error);

    void Calculate(float given_delta);
    
    cl::Image2D& GetWeights(void)
    {
        return this->weights_img;
    }

    void CalcWeightOnHost(void);

    void CopyToImg(void);
};
}

