/*
 * ClSteelThread.h
 *
 *  Created on: Dec 15, 2013
 *      Author: roman
 */

#ifndef CLSTEELTHREAD_H_
#define CLSTEELTHREAD_H_

#define __CL_ENABLE_EXCEPTIONS

#include <CL/cl.hpp>
#include <vector>
#include <string>

class CLSteelThread {
    std::vector<cl::Platform> platforms;
    std::vector<cl::Device> devices;

    cl::Context context;
    cl::CommandQueue q_htod, q_dtoh, q_dtod, q_cmd;

    //Different OpenCL Device properties
    std::string dev_extensions, dev_name, dev_opencl_v, dev_profile, dev_vendor, dev_v, dev_driver_v;
    std::vector<std::size_t> wg_sizes;

public:
    CLSteelThread(void) throw(cl::Error);

    ~CLSteelThread();

    CLSteelThread& operator = (CLSteelThread rhs)
    {
        std::swap(*this, rhs);
        return *this;
    }

    const std::string& GetDeviceName(void) const{return dev_name;}
    const std::string& GetDeviceExtensions (void) const{return dev_extensions;}
    const std::string& GetDeviceOpenCLCVersion (void) const{return dev_opencl_v;}
    const std::string& GetDeviceProfile (void) const{return dev_profile;}
    const std::string& GetDeviceVendor (void) const{return dev_vendor;}
    const std::string& GetDeviceDriverVersion (void) const{return dev_driver_v;}
    const std::string& GetDeviceVersion (void) const{return dev_v;}
    const std::vector<std::size_t>& GetMaxWGSizes (void) const {return wg_sizes;}
    

    const cl::Context& get_Context() const {return context;}
    cl::Context& get_Context() {return context;}

    const cl::CommandQueue& get_Cmd() const {return q_cmd;}
    cl::CommandQueue& get_Cmd() {return q_cmd;}

    const cl::CommandQueue& get_Dtod() const {return q_dtod;}
    cl::CommandQueue& get_Dtod() {return q_dtod;}

    const cl::CommandQueue& get_Dtoh() const {return q_dtoh;}
    cl::CommandQueue& get_Dtoh() {return q_dtoh;}

    const cl::CommandQueue& get_Htod() const {return q_htod;}
    cl::CommandQueue& get_Htod() {return q_htod;}

    const std::vector<cl::Device>& get_Devices() const {return devices;}
    std::vector<cl::Device>& get_Devices() {return devices;}
};

#endif /* CLSTEELTHREAD_H_ */
