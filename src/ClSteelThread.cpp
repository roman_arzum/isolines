/*
 * ClSteelThread.cpp
 *
 *  Created on: Dec 15, 2013
 *      Author: roman
 */

#include "CLSteelThread.hpp"

CLSteelThread::CLSteelThread()
{
	try {
		cl::Platform::get(&platforms);
		platforms[0].getDevices(CL_DEVICE_TYPE_GPU, &devices);

		context = cl::Context(devices);
		q_htod  = cl::CommandQueue(context, devices[0]);
		q_dtoh  = cl::CommandQueue(context, devices[0]);
		q_dtod  = cl::CommandQueue(context, devices[0]);
		q_cmd   = cl::CommandQueue(context, devices[0]);

		//Collect some Device properties
		this->devices[0].getInfo(CL_DEVICE_MAX_WORK_ITEM_SIZES, &this->wg_sizes);
		this->devices[0].getInfo(CL_DEVICE_EXTENSIONS, &this->dev_extensions);
		this->devices[0].getInfo(CL_DEVICE_NAME, &this->dev_name);
		this->devices[0].getInfo(CL_DEVICE_OPENCL_C_VERSION, &this->dev_opencl_v);
		this->devices[0].getInfo(CL_DEVICE_PROFILE, &this->dev_profile);
		this->devices[0].getInfo(CL_DEVICE_VENDOR, &this->dev_vendor);
		this->devices[0].getInfo(CL_DEVICE_VERSION, &this->dev_v);
		this->devices[0].getInfo(CL_DRIVER_VERSION, &this->dev_driver_v);
	}
	catch(cl::Error &e) { throw e;}
}

CLSteelThread::~CLSteelThread()
{
}