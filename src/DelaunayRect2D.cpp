#include "DelaunayRect2D.hpp"
#include "triangle.h"

#include <sstream>
#include <fstream>

#include <opencv2/opencv.hpp>

namespace delaunay
{

DelaunayRect2D::DelaunayRect2D(void):
    points(4)
{
    points[0] = Point2D();
    points[1] = Point2D();
    points[2] = Point2D();
    points[3] = Point2D();
    this->max_value = 0.0f;
}

DelaunayRect2D::DelaunayRect2D(
    const Point2D &l_up, 
    const Point2D &r_down):

    //We have 4 points at least - vertices of rectangle
    points(4)
{
    this->origin = l_up;
    this->region = r_down;

    points[0] = this->origin;
    points[1] = Point2D(r_down.get_X(), l_up.get_Y());
    points[2] = Point2D(l_up.get_X(), r_down.get_Y());
    points[3] = this->region;

    //Find maximal depth value
    std::vector<float> depths;
    depths.push_back(points[0].get_Depth());
    depths.push_back(points[1].get_Depth());
    depths.push_back(points[2].get_Depth());
    depths.push_back(points[3].get_Depth());
    std::sort(depths.begin(), depths.end());
    this->max_value = depths[3];
}

DelaunayRect2D::DelaunayRect2D(const DelaunayRect2D &other, const float scale_factor)
{
    this->max_value = other.max_value;
    this->points = other.points;

    this->origin = Point2D(other.origin.x/scale_factor, other.origin.y/scale_factor);
    this->region = Point2D(other.region.x/scale_factor, other.region.y/scale_factor);

    for(unsigned int i=0; i<other.points.size(); i++){
        this->points[i].x = other.points[i].x / scale_factor;
        this->points[i].y = other.points[i].y / scale_factor;
    }
}

DelaunayRect2D::~DelaunayRect2D(void)
{
}

void DelaunayRect2D::ImportPoints(const std::vector<Point2D> &given_points)
{
    std::vector<Point2D>::iterator it = this->points.end()-1;
    this->points.insert(it, given_points.begin(), given_points.end());
}

void DelaunayRect2D::ImportPoints(const std::string &filename)
{
    std::ifstream f_input;
    std::string line;
    
    try{
        f_input.open(filename.c_str(), std::ifstream::in);
    }
    catch (std::ios_base::failure &ios_failure){
        throw ios_failure;
    }

    if(f_input.good()){
        while(getline(f_input, line)){
            int x, y;
            float depth;
            std::stringstream ss(line);
        
            ss >>x>>y>>depth;
            this->points.push_back(Point2D(x, y, depth));
            if(depth > this->max_value) {this->max_value = depth;}
        }
    }
    else{
        std::string err_mesg("No data inp file ");
        err_mesg += filename;
        std::exception bad_input(err_mesg.c_str());
        throw bad_input;
    }
}

std::vector<Point2D> DelaunayRect2D::GetBBox(void)
{
    //First four elements are vertices of bounding box
    return std::vector<Point2D>(this->points.begin(), this->points.begin()+4);
}

void DelaunayRect2D::BuildTriangulation(void)
{
    triangulateio inp, out;

    memset(&inp,  0, sizeof(inp));
    memset(&out, 0, sizeof(out));

    inp.numberofpoints             = this->points.size();
    inp.numberofpointattributes    = 0;
    inp.pointlist                  = (REAL *) malloc(inp.numberofpoints * 2 * sizeof(REAL));
    inp.pointmarkerlist            = (int *) malloc(inp.numberofpoints * sizeof(int));

    for(int i=0; i < inp.numberofpoints; i++){
        inp.pointlist[i*2]   = this->points[i].get_X();
        inp.pointlist[i*2+1] = this->points[i].get_Y();

        inp.pointmarkerlist[i] = i;
    }

    inp.pointattributelist = 
        (REAL *) calloc(inp.numberofpoints * inp.numberofpointattributes, sizeof(REAL));

    inp.numberofsegments = 0;
    inp.numberofholes    = 0;
    inp.numberofregions  = 1;
    inp.regionlist       = (REAL *) malloc(inp.numberofregions * 4 * sizeof(REAL));
    inp.regionlist[0]    = 1.0;
    inp.regionlist[1]    = 1.0;
    inp.regionlist[2]    = 1.0;
    inp.regionlist[3]    = 1.0;

    /* Triangulate the points.  Switches are chosen to  preserve
     * the convex hull (c) & number everything from a zero (z). */
    triangulate("cz", &inp, &out, 0x0);

    printf("Initial triangulation:\n\n");

    for (int i = 0; i < out.numberoftriangles; i++){
        delaunay::Point2D 
            a(this->points[out.trianglelist[i * out.numberofcorners + 0]]), 
            b(this->points[out.trianglelist[i * out.numberofcorners + 1]]), 
            c(this->points[out.trianglelist[i * out.numberofcorners + 2]]);

        this->triangles.push_back(delaunay::DelaunayTriangle2D(a, b, c));
    }

    /* Free all allocated arrays, including those allocated by Triangle. */
    free(inp.pointlist);
    free(inp.pointattributelist);
    free(inp.pointmarkerlist);
    free(inp.regionlist);
    free(out.pointlist);
    free(out.pointattributelist);
    free(out.pointmarkerlist);
    free(out.trianglelist);
    free(out.triangleattributelist);
    free(out.trianglearealist);
    free(out.neighborlist);
    free(out.segmentlist);
    free(out.segmentmarkerlist);
    free(out.edgelist);
    free(out.edgemarkerlist);

    return ;
}

static void DrawLine( cv::Mat &img, cv::Point &start, cv::Point &end )
{
  int thickness = 2;
  int lineType = 8;
  line(img, start, end, cv::Scalar( 0, 0, 0 ), thickness, lineType);
}

void DelaunayRect2D::ShowTrianglulation(void)
{
    cv::Mat img(this->region.get_Y(),this->region.get_X(), CV_32F, cv::Scalar( 5, 5, 5 ));
    
    std::vector<DelaunayTriangle2D>::iterator it = this->triangles.begin();
    for(;it != this->triangles.end(); it++){
        cv::Point start, end;
            
        start = cv::Point(it->vertex1.get_X(), it->vertex1.get_Y());
        end   = cv::Point(it->vertex2.get_X(), it->vertex2.get_Y());
        DrawLine(img, start, end);

        start = cv::Point(it->vertex2.get_X(), it->vertex2.get_Y());
        end   = cv::Point(it->vertex3.get_X(), it->vertex3.get_Y());
        DrawLine(img, start, end);

        start = cv::Point(it->vertex3.get_X(), it->vertex3.get_Y());
        end   = cv::Point(it->vertex1.get_X(), it->vertex1.get_Y());
        DrawLine(img, start, end);
    }

    cv::namedWindow( "Triangulation", cv::WINDOW_NORMAL  );
    cv::imshow("Triangulation", img);
    cv::waitKey(0);
}
};
