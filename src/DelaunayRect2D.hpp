#pragma once
#include "DelaunayTriangle2D.hpp"
#include "Point2D.hpp"
#include <exception>
#include <vector>
#include <string>

namespace bath{
    class BathMap;
    class BathGrad;
}

namespace delaunay
{
class DelaunayRect2D
{
    friend class bath::BathMap;
    friend class bath::BathGrad;

    //Maximal depth value (for normalization)
    float max_value;
    std::vector<Point2D> points;
    std::vector<DelaunayTriangle2D> triangles;
public:
    Point2D origin, region;

    DelaunayRect2D(void);
    DelaunayRect2D(const Point2D &l_up, const Point2D &r_down);
    DelaunayRect2D(const DelaunayRect2D &other, const float scale_factor);

    ~DelaunayRect2D(void);

    DelaunayRect2D& operator = (DelaunayRect2D rhs)
    {
        std::swap(*this, rhs);
        return *this;
    }

    void BuildTriangulation(void);

    void ShowTrianglulation(void);

    void ImportPoints(const std::string &filename);
    void ImportPoints(const std::vector<Point2D> &points);

    const float GetMaxVal(void) const {return this->max_value;}
    
    const std::size_t GetAreaSize(void) const { return 
        (this->region.get_X() - this->origin.get_X()) * 
        (this->region.get_Y() - this->origin.get_Y()); }

    std::size_t GetWidth(void) const
    {
        return region.get_X();
    }

    std::size_t GetHeight(void) const
    {
        return region.get_Y();
    }

    //Return vertices of rectangle area
    std::vector<Point2D> GetBBox(void);

    std::vector<Point2D>& GetPoints(void) {return points;}

    const std::vector<Point2D>& GetPoints(void) const {return points;}

};
};

