#include "DelaunayTriangle2D.hpp"
#include "DelaunayRect2D.hpp"
#include "Vector2D.hpp"
#include "Point2D.hpp"
#include <algorithm>
#include <vector>
namespace delaunay
{

DelaunayTriangle2D::DelaunayTriangle2D(void):
    vertex1(),
    vertex2(),
    vertex3()
{}

DelaunayTriangle2D::DelaunayTriangle2D(
    const Point2D &p1, 
    const Point2D &p2, 
    const Point2D &p3):

    vertex1(p1),
    vertex2(p2),
    vertex3(p3)
{}

DelaunayTriangle2D::~DelaunayTriangle2D(void)
{}

static bool comp_x(const Point2D &p1, const Point2D &p2)
{
    //X coordinate has first priority, then Y coordinate
    if(p1.get_X() < p2.get_X()){
        return true;
    }
    else if(p1.get_X() == p2.get_X()){
        return (p1.get_Y() < p2.get_Y());
    }
    else{
        return false;
    }
}

static bool comp_y(const Point2D &p1, const Point2D &p2)
{
    return (p1.get_Y() > p2.get_Y());
}

bool DelaunayTriangle2D::PointInside(const Point2D &p) const
{
    Point2D a(vertex1), b(vertex2), c(vertex3);
    
    Vector2D
        ap = p-a,
        bp = p-b,
        cp = p-c;
    
    Point2D
        d = ap.Intersection(c-b),
        e = bp.Intersection(a-c),
        f = cp.Intersection(b-a);

    Vector2D
        ad = d-a,
        be = e-b,
        cf = f-c;

    if( (ap.Get_Length() <= ad.Get_Length()) && 
        (bp.Get_Length() <= be.Get_Length()) && 
        (cp.Get_Length() <= cf.Get_Length()) ){
        return true;
    } else{
        return false;
    }
}

bool DelaunayTriangle2D::PointIsVertex(const Point2D &p) const
{
    Point2D
        a(this->vertex1),
        b(this->vertex2),
        c(this->vertex3);

    bool p_is_vertex =
        ((p.x == a.x) && (p.y == a.y)) ||
        ((p.x == b.x) && (p.y == b.y)) ||
        ((p.x == c.x) && (p.y == c.y));

    return p_is_vertex;
}

float DelaunayTriangle2D::TPG(const Point2D &p) const
{
    const Point2D 
        &a(this->vertex1), 
        &b(this->vertex2), 
        &c(this->vertex3);

    if ((p.x == a.x) && (p.y == a.y)) { return a.depth; }
    if ((p.x == b.x) && (p.y == b.y)) { return b.depth; }
    if ((p.x == c.x) && (p.y == c.y)) { return c.depth; }

    Vector2D
        ap = p-a,
        bp = p-b,
        cp = c-p,
        da = ap.Intersection(b-c) - a,
        eb = bp.Intersection(c-a) - b,
        fc = cp.Intersection(a-b) - c,
        dp = da.end - p,
        ep = eb.end - p,
        fp = fc.end - p;

    float
        prox_a = (float)(dp.Get_Length() / da.Get_Length()),
        prox_b = (float)(ep.Get_Length() / eb.Get_Length()),
        prox_c = (float)(fp.Get_Length() / fc.Get_Length());

    return prox_a * a.depth + prox_b * b.depth + prox_c * c.depth;
}

float DelaunayTriangle2D::WeightTPG(const Point2D &p) const
{
    Point2D 
        a(this->vertex1), 
        b(this->vertex2), 
        c(this->vertex3);

    //If given point is triangle vertex, no need to do calculations
    bool p_is_vertex = 
        ((p.x == a.x) && (p.y == a.y)) ||
        ((p.x == b.x) && (p.y == b.y)) ||
        ((p.x == c.x) && (p.y == c.y));

    if(p_is_vertex) {return 1.0f;}

    //Vertices are defined with absolute confidency
    a.depth = b.depth = c.depth = 1.0f;

    Vector2D
        ap = p-a,
        bp = p-b,
        cp = c-p,
        da = ap.Intersection(b-c) - a,
        eb = bp.Intersection(c-a) - b,
        fc = cp.Intersection(a-b) - c,
        dp = da.end - p,
        ep = eb.end - p,
        fp = fc.end - p;

    double
        prox_a = dp.Get_Length() / da.Get_Length(),
        prox_b = ep.Get_Length() / eb.Get_Length(),
        prox_c = fp.Get_Length() / fc.Get_Length();

    //Avoid returning 1.0f anywhere except vertices
    return (float)(prox_a * a.depth + prox_b * b.depth + prox_c * c.depth)*0.95;
}

const DelaunayRect2D DelaunayTriangle2D::GetBBox(void) const
{
    int
        left_most,
        right_most,
        up_most,
        down_most;

    //Sort X coordinates
    std::vector<int> vertices(3);
    vertices[0] = this->vertex1.get_X();
    vertices[1] = this->vertex2.get_X();
    vertices[2] = this->vertex3.get_X();
    std::sort(vertices.begin(), vertices.end());
    
    left_most  = vertices[0];
    right_most = vertices[2];

    //Sort Y coordinates
    vertices[0] = this->vertex1.get_Y();
    vertices[1] = this->vertex2.get_Y();
    vertices[2] = this->vertex3.get_Y ();
    std::sort(vertices.begin(), vertices.end());

    up_most   = vertices[0];
    down_most = vertices[2];

    Point2D origin(left_most, up_most), region(right_most, down_most);
    return DelaunayRect2D(origin, region);
}

};
