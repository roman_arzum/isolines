#pragma once
#include "Vector2D.hpp"
#include "Point2D.hpp"
#include <utility>

namespace delaunay
{
class DelaunayRect2D;

class DelaunayTriangle2D
{
    friend class DelaunayRect2D;

    Point2D 
        vertex1,
        vertex2,
        vertex3;
public:
    DelaunayTriangle2D(void);
    DelaunayTriangle2D(const Point2D &p1, const Point2D &p2, const Point2D &p3);

    ~DelaunayTriangle2D(void);

    DelaunayTriangle2D& operator = (DelaunayTriangle2D rhs)
    {
        std::swap(*this, rhs);
        return *this;
    }

    //Return true if point lays inside triangle
    bool PointInside(const Point2D &p) const;

    bool PointIsVertex(const Point2D &p) const;

    //Return three-points gradient in given point
    float TPG(const Point2D &p) const;

    //Return point weight (1 - fixed point, 0 - value unpredictable)
    float WeightTPG(const Point2D &p) const;

    //Return bounding box
    const DelaunayRect2D GetBBox(void) const;
};
};

