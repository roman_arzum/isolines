typedef struct AnchorPoint
{
	int2 xy;
	float depth;
} AnchorPoint;

void __kernel ImportAnchors(
	__global const AnchorPoint	*anchors,
	__write_only image2d_t		output,
	const float					normalize_factor)
{
	const int2 img_dims = get_image_dim(output);
	
	AnchorPoint a_point = anchors[get_global_id(0)];
	float4 color = a_point.depth / normalize_factor;
	write_imagef(output, a_point.xy, color);

	return;
}

int to_1d(int2 position, int linesize)
{
	return position.x + position.y * linesize;
}

__constant sampler_t ImgSampler = 
	CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;

void __kernel RestoreImage(
	__read_only image2d_t		input,
	__read_only image2d_t		weights,
	__write_only image2d_t		output,
	__write_only image2d_t		deltas,
	__read_only image2d_t		grad_x,
	__read_only image2d_t		grad_y)
{
	int2 pos  = {get_global_id(0), get_global_id(1)};
	int
		rows = pos.y / 2,
		cols = pos.x / 2;

	float region[3][3], delta;
	delta = read_imagef(input, ImgSampler, pos).s0;

	//Get depth in 3x3 neighbor region
	for(int row = 0; row < 3; row++){
		for(int col = 0; col < 3; col++){
			/* Get depth in current point of neighbourhood
			 * & check out-of-border IO . */
			int2 
				c_pos = {pos.x + col - 1, pos.y + row - 1},
				c_top = {c_pos.x, c_pos.y - 1},
				c_low = {c_pos.x, c_pos.y + 1},
				c_lft = {c_pos.x - 1, c_pos.y},
				c_rgt = {c_pos.x + 1, c_pos.y};

			float 
				gxl   = read_imagef(grad_x, ImgSampler, c_lft).s0,
				gxc   = read_imagef(grad_x, ImgSampler, c_pos).s0,
				gyt   = read_imagef(grad_y, ImgSampler, c_top).s0,
				gyc   = read_imagef(grad_y, ImgSampler, c_pos).s0,

				dxl   = read_imagef(input, ImgSampler, c_lft).s0,
				dxr	  = read_imagef(input, ImgSampler, c_rgt).s0,
				dyt   = read_imagef(input, ImgSampler, c_top).s0,
				dyl   = read_imagef(input, ImgSampler, c_low).s0;
				
			region[row][col] = (dxl + gxl + dxr - gxc + dyt + gyt + dyl - gyc) / 4.0f;
		}
	}

	bool 
		anchor_point = (read_imagef(weights, ImgSampler, pos).s0 == 1.0f),
		shore_point  = (read_imagef(weights, ImgSampler, pos).s0 == 0.0f);
	
	//Average depths & write as output
	if(!anchor_point && !shore_point){
		float4 avg_depth = (
			region[0][0] + region[0][1] + region[0][2] + 
			region[1][0] +                region[1][2] +
			region[2][0] + region[2][1] + region[2][2]) / 8;

		write_imagef(output, pos, avg_depth);

		delta -= avg_depth.s0;
		write_imagef(deltas, pos, delta);
	}
	
	return;
}