#include "GradientImage.hpp"
#include "CLSteelThread.hpp"
#include "BathMap.hpp"

#include <utility>
#include <fstream>
#include <iostream>

using namespace bath;
using namespace std;

const float GradientImage::delta = 5e-7f;

GradientImage::GradientImage(
        CLSteelThread   &steel_thread,
        cl::Buffer		&ref_grad_x,
        cl::Buffer		&ref_grad_y,
        cl::Image2D		&weights,
        BathMap			&bath_map):

        st_thread(steel_thread),
        p_weights(&weights),
        map(bath_map)
{
    try{
        weights.getImageInfo(CL_IMAGE_WIDTH, &this->width);
        weights.getImageInfo(CL_IMAGE_HEIGHT, &this->height);

        this->mat = cv::Mat(this->height, this->width, CV_32F, cv::Scalar(0.0f));

        this->grad_x = cl::Image2D(steel_thread.get_Context(), 
            CL_MEM_ALLOC_HOST_PTR , cl::ImageFormat(CL_LUMINANCE, CL_FLOAT), 
            this->mat.cols, this->mat.rows);

        this->grad_y = cl::Image2D(steel_thread.get_Context(), 
            CL_MEM_ALLOC_HOST_PTR , cl::ImageFormat(CL_LUMINANCE, CL_FLOAT), 
            this->mat.cols, this->mat.rows);

        try{
            cl::size_t<3> origin, region;
            origin[0] = origin[1] = origin[2] = 0;
            region[0] = this->width;
            region[1] = this->height;
            region[2] = 1;

            this->st_thread.get_Htod().enqueueCopyBufferToImage(ref_grad_x, 
                this->grad_x, 0, origin, region);

            this->st_thread.get_Htod().enqueueCopyBufferToImage(ref_grad_y, 
                this->grad_y, 0, origin, region);
        }catch (cl::Error &e) {throw e;}
        
        for(int i=0; i<2; i++){
            this->interim[i] = cl::Image2D(steel_thread.get_Context(), 
                CL_MEM_ALLOC_HOST_PTR | CL_MEM_COPY_HOST_PTR, cl::ImageFormat(CL_LUMINANCE, CL_FLOAT), 
                this->mat.cols, this->mat.rows, 0, (void*)this->mat.data);
        }

        this->deltas = cl::Image2D(steel_thread.get_Context(), 
            CL_MEM_ALLOC_HOST_PTR , cl::ImageFormat(CL_LUMINANCE, CL_FLOAT), 
            this->mat.cols, this->mat.rows);

        //Later, these pointers will be swapped
        this->p_out = &this->interim[0];
        this->p_in = this->p_out;

        //Create kernels
        std::ifstream src_file_in("../GradientImage.cl");
        std::string src_code(std::istreambuf_iterator<char>(src_file_in), (std::istreambuf_iterator<char>()));
        
        this->src = cl::Program::Sources(1, std::make_pair(src_code.c_str(), src_code.length() + 1));
        this->prg = cl::Program(this->st_thread.get_Context(), src);
        this->prg.build(st_thread.get_Devices(), "");

        this->k_iteration = cl::Kernel(this->prg, "RestoreImage");
        this->k_import_anchors = cl::Kernel(this->prg, "ImportAnchors");
    } catch(cl::Error &e) {
        //Get programm build log (shows OpenCL source code errors)
        if(e.err() == CL_BUILD_PROGRAM_FAILURE){
            std::cerr <<"Build log:"<<std::endl<<
            this->prg.getBuildInfo<CL_PROGRAM_BUILD_LOG>(st_thread.get_Devices()[0])
            <<std::endl;
        }

        throw e;
    }
}

GradientImage::~GradientImage(void)
{}

GradientImage& GradientImage::operator = (GradientImage rhs)
{
    swap(rhs, *this);
    return *this;
}

void GradientImage::RestoreFromGrad(void)
{
    float curr_delta = 1.0f;
    int it = 0;

    while(curr_delta >= this->delta){
        //Swap input & output to make round-robin iteration process
        this->p_in = this->p_out;
        this->p_out = (this->p_out == &this->interim[0])?
            (this->p_out = &this->interim[1]): 
            (this->p_out = &this->interim[0]);

        //Run kernels
        try{
            this->k_iteration.setArg<cl::Image2D>(0, *this->p_in);
            this->k_iteration.setArg<cl::Image2D>(1, *this->p_weights);
            this->k_iteration.setArg<cl::Image2D>(2, *this->p_out);
            this->k_iteration.setArg<cl::Image2D>(3, this->deltas);
            this->k_iteration.setArg<cl::Image2D>(4, this->grad_x);
            this->k_iteration.setArg<cl::Image2D>(5, this->grad_y);

            cl::Event evt;
            this->st_thread.get_Cmd().enqueueNDRangeKernel(this->k_iteration, 
                cl::NullRange, cl::NDRange(this->width, this->height), cl::NullRange,
                NULL, &evt);
            evt.wait();

            cl::size_t<3> origin, region;
            origin[0] = origin[1] = origin[2] = 0;
            region[0] = this->width;
            region[1] = this->height;
            region[2] = 1;

            std::vector<float> host_deltas(this->mat.cols * this->mat.rows, 0.0f);
            this->st_thread.get_Htod().enqueueReadImage(this->deltas,
                CL_TRUE, origin, region, 0, 0, (void*)&host_deltas[0]);
            std::sort(host_deltas.begin(), host_deltas.end());
            curr_delta = *(host_deltas.end()-1);

        } catch(cl::Error &e) {throw e;}

        it++;
        cout <<"Restoring from gradients. Iteration: "<<it<<". Delta "<<curr_delta<<"/"<<this->delta<<endl;

        //this->ShowOutput();
        if (it >= 16384){
            break;
        }
    }
    this->ShowOutput();
}

void GradientImage::ShowOutput(void)
{
    try{
        cl::size_t<3> origin, region;
        origin[0] = origin[1] = origin[2] = 0;
        region[0] = this->width;
        region[1] = this->height;
        region[2] = 1;

        this->st_thread.get_Htod().enqueueReadImage(*this->p_out,
            CL_TRUE, origin, region, 0, 0, (void*)this->mat.data);
    }catch (cl::Error &e) {throw e;}

    cv::namedWindow("Restored Image", cv::WINDOW_NORMAL);
    cv::imshow("Restored Image", this->mat);
    cv::waitKey(0);
}

void GradientImage::ShowWeights(void)
{
    cv::Mat w_mat(this->height, this->width, CV_32F, cv::Scalar(0.0f));
    try{
        cl::size_t<3> origin, region;
        origin[0] = origin[1] = origin[2] = 0;
        region[0] = this->width;
        region[1] = this->height;
        region[2] = 1;

        this->st_thread.get_Htod().enqueueReadImage(*this->p_weights,
            CL_TRUE, origin, region, 0, 0, (void*)w_mat.data);
    }catch (cl::Error &e) {throw e;}

    cv::namedWindow("Weights", cv::WINDOW_NORMAL);
    cv::imshow("Weights", w_mat);
    cv::waitKey(0);
}

void GradientImage::ImportAnchors(void)
{
    try{
        float factor = this->map.delaunay_area.GetMaxVal();

        this->k_import_anchors.setArg<cl::Buffer>(0, this->map.points_buf);
        this->k_import_anchors.setArg<cl::Image2D>(1, *this->p_in);
        this->k_import_anchors.setArg(2, sizeof(float), &factor);

        //Import anchor points into both images
        cl::Event evt;
        this->st_thread.get_Cmd().enqueueNDRangeKernel(this->k_import_anchors,
            cl::NullRange, cl::NDRange(this->map.delaunay_area.GetPoints().size()),
            cl::NullRange, NULL, &evt);
        evt.wait();

        this->k_import_anchors.setArg<cl::Image2D>(1, *this->p_out);
        this->st_thread.get_Cmd().enqueueNDRangeKernel(this->k_import_anchors,
            cl::NullRange, cl::NDRange(this->map.delaunay_area.GetPoints().size()),
            cl::NullRange, NULL, &evt);
        evt.wait();

    }catch(cl::Error &e) {throw e;}
}