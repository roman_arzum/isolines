#pragma once
#define __CL_ENABLE_EXCEPTIONS

#include <opencv2/opencv.hpp>
#include <CL/cl.hpp>
#include <vector>
#include <string>

namespace bath{
    class BathMap;
}

class CLSteelThread;

class GradientImage
{
protected:
    cl::Image2D 
        interim[2],
        grad_x,
        grad_y,
        deltas,
        *p_in,
        *p_out,
        *p_weights;

    cv::Mat mat;

    cl::Kernel k_iteration, k_import_anchors;
    cl::Program::Sources src;
    cl::Program prg;

    static const float delta;

    CLSteelThread &st_thread;
    bath::BathMap &map;

    std::size_t width, height;

public:
    GradientImage(
        CLSteelThread   &steel_thread,
        cl::Buffer		&ref_grad_x,
        cl::Buffer		&ref_grad_y,
        cl::Image2D		&weights,
        bath::BathMap	&bath_map);

    ~GradientImage(void);

    GradientImage& operator = (GradientImage rhs);

    void ShowOutput(void);

    void ShowWeights(void);

    void ImportAnchors(void);

    void RestoreFromGrad(void);
};

