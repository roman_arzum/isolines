#include "ImgPyramid.hpp"

#include <sstream>
#include <math.h>
#include <omp.h>

using namespace delaunay;

namespace bath
{
ImgPyramid::ImgPyramid(
        const float				given_factor,
        const std::size_t		num_planes, 
        const DelaunayRect2D	&root_area,
        CLSteelThread			&st_thread,
        cv::Mat					&shore_mask):
        factor(given_factor)
{
    for(std::size_t i=0; i < num_planes; i++){
        float curr_factor = pow(this->factor, i);

        DelaunayRect2D rect(root_area, curr_factor);
        //Triangulation is needed for all planes for further
        //gradient image reconstruction.
        rect.BuildTriangulation();
        this->areas.push_back(rect);
    }

    for(std::size_t i=0; i < num_planes; i++){
        float 
            curr_factor = pow(this->factor, i),
            pl_delta = BathMap::delta * curr_factor;

        //Make shore mask of appropriate sizes
        if (curr_factor != 1){
            int
                cols = shore_mask.cols / curr_factor,
                rows = shore_mask.rows / curr_factor;
            
            cv::Mat mask(rows, cols, CV_8U);
            cv::resize(shore_mask, mask, cv::Size2i(cols, rows), 0, 0, cv::INTER_NEAREST);
            
            BathMap my_map(mask, this->areas[i], st_thread);
            this->planes.push_back(Plane(my_map, pl_delta));
        }
        else{
            BathMap my_map(shore_mask, this->areas[i], st_thread);
            this->planes.push_back(Plane(my_map, pl_delta));
        }
    }

    for (std::size_t i = 0; i < num_planes; i++){
        BathGrad my_grad(st_thread, this->areas[i], this->planes[i].map);
        this->grads.push_back(my_grad);
    }
}

ImgPyramid::~ImgPyramid(void)
{}

void ImgPyramid::ProcessPlanes(bool save_results)
{
    //Make initial approximation for smallest plane
    std::vector<Plane>::iterator it = this->planes.end() - 1;

    it->map.MakeInitApprox();
    it->map.CalcWeightOnHost();
    it->map.Calculate(it->delta);
    it->map.CopyToImg();
    it->map.SaveToMat();

    /* Now make extrapolation from smallest image to largest,
    * using small image as initial data for large one. First
    * plane is ready, so we skip it. */
    std::size_t num_plane = this->planes.size() - 2;
    std::vector<Plane>::reverse_iterator r_it = this->planes.rbegin() + 1;
    std::vector<BathGrad>::reverse_iterator r_git = this->grads.rbegin() + 1;

    for (; r_it != this->planes.rend(); r_it++){
        try{
            this->ExportFromPrevLayer(r_it);
            
            //Processing initial approximation
            r_it->map.CalcWeightOnHost();
            r_it->map.Calculate(r_it->delta);
            r_it->map.CopyToImg();
            r_it->map.SaveToMat();
            
            //And gradients calculation
            r_git->Calculate(r_it->delta);

            if (save_results){
                std::stringstream 
                    filename_map,
                    filename_gradX, filename_gradY;
                
                filename_map << "Approx_Plane" << num_plane << ".png";
                filename_gradX << "GradX_Plane" << num_plane << ".png";
                filename_gradY << "GradY_Plane" << num_plane << ".png";
                
                this->SaveToFile(num_plane, filename_map.str());
                r_git->SaveToFile(filename_gradX.str(), filename_gradY.str());
            }
            num_plane--;
        }
        catch (cl::Error &e) { throw e; }
        r_git++;
    }
}

void ImgPyramid::ExportFromPrevLayer(std::vector<Plane>::reverse_iterator it)
{
    //Use previous layer bath as initial value for current
    cv::resize((it - 1)->map.cv_img, it->map.cv_img, cv::Size(it->map.cv_img.cols, it->map.cv_img.rows), cv::INTER_CUBIC);
    
    //Write result to GPU
    try{
        it->map.st_thread.get_Htod().enqueueWriteBuffer(it->map.interim_buffer,
            CL_TRUE, 0, it->map.cv_img.cols * it->map.cv_img.rows * sizeof(float),
            (const void*)it->map.cv_img.data);
    }catch (cl::Error &e) {throw e;}
}

void ImgPyramid::Show(void)
{
    std::vector<Plane>::iterator it = this->planes.begin();
    it->map.Show();
}

void ImgPyramid::SaveToFile(
    const std::size_t   num_plane, 
    std::string         &filename)
{
    if (num_plane > this->planes.size()){
        cl::Error e(2, "Invalid plane number");
        CryErr(e);
        throw e;
    }

    std::vector<Plane>::iterator it = this->planes.begin() + num_plane;
    cv::Mat img(it->map.cv_img.rows, it->map.cv_img.cols, CV_8U);

    #pragma omp parallel for
    for (std::size_t i = 0; i<it->map.cv_img.rows * it->map.cv_img.cols; i++){
        img.data[i] = ((float*)it->map.cv_img.data)[i] * 255;
    }

    #pragma omp barrier
    cv::imwrite(filename, img);
}

cl::Image2D& ImgPyramid::GetWeights(void)
{
    return this->planes.begin()->map.GetWeights();
}

std::size_t ImgPyramid::GetSize(void) const
{
    return this->grads.size();
}

}