#pragma once

#include "DelaunayRect2D.hpp"
#include "BathGrad.hpp"
#include "BathMap.hpp"

#include <vector>
#include <string>
#include <map>

namespace cv{
    class Mat;
}

namespace bath
{
struct Plane
{
    BathMap map;
    const float delta;

    Plane(
        BathMap		&rf_map, 
        float		given_delta):
        
        map(rf_map),
        delta(given_delta)
    {}

    ~Plane()
    {}
};

class ImgPyramid
{
protected:
    float factor;

public:
    std::vector<Plane> planes;
    std::vector<BathGrad> grads;
    std::vector<delaunay::DelaunayRect2D> areas;

    ImgPyramid(
        const float						given_factor,
        const std::size_t				num_planes, 
        const delaunay::DelaunayRect2D	&root_area,
        CLSteelThread					&st_thread,
        cv::Mat							&shore_mask);
    
    ~ImgPyramid(void);

    ImgPyramid& operator = (ImgPyramid rhs)
    {
        std::swap(*this, rhs);
        return *this;
    }

    void ProcessPlanes(bool save_results);

    void ExportFromPrevLayer(std::vector<Plane>::reverse_iterator it);

    void Show(void);

    void SaveToFile(const std::size_t num_plane, std::string &filename);

    std::size_t GetSize(void) const;

    BathMap& GetTopMap(void) {return planes.begin()->map;}

    cl::Image2D& GetWeights(void);
};
}
