#include "Point2D.hpp"
#include "Vector2D.hpp"
namespace delaunay
{

Point2D::Point2D(void)
{
	this->x = 0;
	this->y = 0;
    this->depth = 0.0f;
}

Point2D::Point2D(const Point2D &p)
{
	this->x = p.get_X();
	this->y = p.get_Y();
    this->depth = p.depth;
}

Point2D::Point2D(int p_x, int p_y)
{
	this->x = p_x;
	this->y = p_y;
    this->depth = 0.0f;
}

Point2D::Point2D(int p_x, int p_y, float f_depth)
{
	this->x = p_x;
	this->y = p_y;
    this->depth = f_depth;
}

Point2D::~Point2D(void)
{
}

Vector2D operator-(Point2D &p1, Point2D &p2)
{
    return Vector2D (p2, p1);
}

Vector2D operator-(const Point2D &p1, const Point2D &p2)
{
    return Vector2D (p2, p1);
}
};
