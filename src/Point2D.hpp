#pragma once
#include <utility>
namespace delaunay
{
class Vector2D;

class Point2D
{
    friend class DelaunayTriangle2D;
    friend class DelaunayRect2D;
    friend class Vector2D;

    int x, y;
    float depth;
public:
    Point2D(void);
    Point2D(const Point2D &p);
    Point2D(int p_x, int p_y);
    Point2D(int p_x, int p_y, float f_depth);
    
    ~Point2D(void);

    const int get_X(void) const {return this->x;}
    const int get_Y(void) const {return this->y;}
    const float get_Depth(void) const {return this->depth;}
};

Vector2D operator -(Point2D &p1, Point2D &p2);
Vector2D operator -(const Point2D &p1, const Point2D &p2);
};

