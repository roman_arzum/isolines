#pragma once
#define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.hpp>
#include <string>
#include <iostream>

inline void CryErr(cl::Error &e)
{
    std::cerr << __FILE__ << " " << __LINE__ << " " << e.what() << " " << e.err() << std::endl;
}