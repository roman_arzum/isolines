#include "Vector2D.hpp"
#include <math.h>
namespace delaunay
{

Vector2D::Vector2D(void)
{
	this->begin = Point2D(0, 0);
	this->end = Point2D(0, 0);
}

Vector2D::Vector2D(const Point2D &p1, const Point2D &p2)
{
	this->begin = p1;
	this->end = p2;
}

Vector2D::~Vector2D(void)
{
}

double Vector2D::Get_Length(void) const
{
    return sqrt(
        pow((this->begin.get_X() - this->end.get_X()) * 1.0, 2) +
        pow((this->begin.get_Y() - this->end.get_Y()) * 1.0, 2) );
}

void Vector2D::PutInCenter(void)
{
    this->end.x -= this->begin.get_X();
    this->end.y -= this->begin.get_Y();
    this->begin = Point2D();
}

bool Vector2D::AtLeft(const Point2D &p1)
{
    Vector2D a(*this), b = p1-this->begin;
    a.PutInCenter();
    b.PutInCenter();

    // If pair is at left side, vector multiplication will be positive.
    return (a.end.get_X() * b.end.get_Y() - a.end.get_Y() * b.end.get_X()) > 0;
}

const Point2D Vector2D::Intersection(const Vector2D& other)
{
    float
        d = (other.end.y-other.begin.y) * (this->end.x-this->begin.x) - (other.end.x-other.begin.x) * (this->end.y-this->begin.y),
        a = (other.end.x-other.begin.x) * (this->begin.y-other.begin.y) - (other.end.y-other.begin.y) * (this->begin.x-other.begin.x);

    return Point2D(
        this->begin.x + a * (this->end.x-this->begin.x)/d,
        this->begin.y + a * (this->end.y-this->begin.y)/d);
}
};