#pragma once
#include "Point2D.hpp"
#include <utility>

namespace delaunay
{
class Point2D;

class Vector2D
{
    friend class DelaunayTriangle2D;

    Point2D begin, end;
public:
    Vector2D(void);
    Vector2D(const Point2D &p1, const Point2D &p2);

    ~Vector2D(void);

    Vector2D& operator = (Vector2D rhs)
    {
        std::swap(*this, rhs);
        return *this;
    }

    double Get_Length(void) const;

    //returns true if point lays at the left of vector, false otherwise
    bool AtLeft(const Point2D &p);

    const Point2D& Get_Begin() const;
    const Point2D& Get_End() const;

    const Point2D Intersection(const Vector2D& other);

    Vector2D operator -(const Vector2D &v2);
    Vector2D operator +(const Vector2D &v2);

    void PutInCenter(void);
};
};
