#include "BathGrad.hpp"
#include "BathMap.hpp"
#include "Point2D.hpp"
#include "Vector2D.hpp"
#include "DelaunayRect2D.hpp"
#include "DelaunayTriangle2D.hpp"
#include "CLSteelThread.hpp"
#include "ImgPyramid.hpp"
#include "GradientImage.hpp"

#include <opencv2/opencv.hpp>
#include <CL/cl.hpp>

#include <iostream>
#include <fstream>
#include <vector>
#include <time.h>

int main(int argc, char *argv[])
{
    using namespace std;

    clock_t t = clock();
    try {
        CLSteelThread st_thread;
        cv::Mat shore_mask = cv::imread("../Genichesk.jpg", CV_LOAD_IMAGE_GRAYSCALE);

        delaunay::Point2D p1(0, 0), p2(shore_mask.cols, shore_mask.rows);
        delaunay::DelaunayRect2D area(p1, p2);
        area.ImportPoints("../Genichesk.pointsX.txt");

        bath::ImgPyramid pyramid(1.5f, 6, area, st_thread, shore_mask);
        pyramid.ProcessPlanes(true);

        GradientImage grad_img(st_thread, pyramid.grads[2].GetGradX(), 
            pyramid.grads[2].GetGradY(), pyramid.planes[2].map.GetWeights(), 
            pyramid.planes[2].map);

        grad_img.ImportAnchors();
        grad_img.RestoreFromGrad();
        grad_img.ShowOutput();
    } catch (std::exception &e) {
        std::cerr<<e.what()<<std::endl;
    }

    t = clock() - t;
    std::cout <<"Execution time: "<<((float)t)/CLOCKS_PER_SEC<<std::endl;

    return 0;
}